## Motivation  

The **ECO Platform API** provides developers with seamless access to high-quality, standardized Environmental Product Declaration (EPD) data. As the construction sector moves towards **low-carbon solutions and resource efficiency**, reliable and scientifically validated data is essential for **LCA modeling, sustainable decision-making, and regulatory compliance**.  

ECO Platform brings together EPD program operators, manufacturers, and sustainability experts to ensure **harmonized, credible, and transparent environmental data**. This API is designed to integrate effortlessly into your **LCA workflows**, enabling automation, data-driven analysis, and innovative sustainability applications.  

By leveraging the ECO Platform API, you can:  
- **Access standardized EPD data** for accurate environmental impact assessments.  
- **Automate LCA calculations** and integrate with existing sustainability tools.  
- **Ensure compliance** with international sustainability standards.  
- **Enhance transparency** in construction and green building projects.  

This documentation will guide you through **authenticating, retrieving, and processing EPD data**, providing practical examples to help you integrate the API into your workflows efficiently.  

### Target Audience  

This documentation is for developers and professionals **familiar with Python, APIs, and well-versed in LCA and EPD data**.  

We also assume that you:  
✅ **Have Python set up** ([Setup guide](#python-setup))  
✅ **Have an ECO Platform API key** ([Get your key here](#getting-api-key))


```python
!pip install pandoc requests dotenv
```

    Requirement already satisfied: pandoc in ./py_env/lib/python3.12/site-packages (2.4)
    Requirement already satisfied: requests in ./py_env/lib/python3.12/site-packages (2.32.3)
    Requirement already satisfied: dotenv in ./py_env/lib/python3.12/site-packages (0.9.9)
    Requirement already satisfied: plumbum in ./py_env/lib/python3.12/site-packages (from pandoc) (1.9.0)
    Requirement already satisfied: ply in ./py_env/lib/python3.12/site-packages (from pandoc) (3.11)
    Requirement already satisfied: charset-normalizer<4,>=2 in ./py_env/lib/python3.12/site-packages (from requests) (3.4.0)
    Requirement already satisfied: idna<4,>=2.5 in ./py_env/lib/python3.12/site-packages (from requests) (3.10)
    Requirement already satisfied: urllib3<3,>=1.21.1 in ./py_env/lib/python3.12/site-packages (from requests) (2.2.3)
    Requirement already satisfied: certifi>=2017.4.17 in ./py_env/lib/python3.12/site-packages (from requests) (2024.8.30)
    Requirement already satisfied: python-dotenv in ./py_env/lib/python3.12/site-packages (from dotenv) (1.0.1)



```python
import requests
import pandas as pd
```

### Load the Token


```python
# past you token here
TOKEN = "CHANGE ME TO THE TOKEN"

# or load form env
if TOKEN.startswith("CHANGE"):
    import os
    from dotenv import load_dotenv

    load_dotenv()
    # load the Access Token
    TOKEN = os.getenv('TOKEN')

# Verify token is set
if not TOKEN:
    raise Exception("Error: TOKEN is not set. Please set the TOKEN variable.")
```

## Get Global Warming Potential (GWP) of Concrete

### Step 1: Find the Concrete 


```python

# Base URL for the Eco Platform data aggregator
BASE_URL = 'https://data.eco-platform.org/resource/'

# API endpoint for fetching process data
PROCESS_ENDPOINT = 'processes'

# Query parameters for filtering concrete data
query_params = {
    'search': 'true',
    'distributed': 'true',
    'virtual': 'true',
    'metaDataOnly': 'false',
    'validUntil': '2024',
    'pageSize': 5,  
    'startIndex': 0,
    'format': 'JSON',
}
headers = {
    'Authorization': f'Bearer {TOKEN}'  
}

# Specify the name of the concrete mix to search for
query_params["name"] = "Concrete C25/30 XC4 XF1 XA1 F3 16 M ECOPact , Mix number DA4234-DHFS Version 1"

# Send GET request to fetch relevant concrete data
response = requests.get(f'{BASE_URL}{PROCESS_ENDPOINT}', headers=headers, params=query_params)
response.raise_for_status()

# Convert response data into a structured DataFrame
concrete_data_df = pd.DataFrame(response.json()["data"])
concrete_data_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>languages</th>
      <th>name</th>
      <th>uuid</th>
      <th>version</th>
      <th>geo</th>
      <th>classific</th>
      <th>classificSystem</th>
      <th>type</th>
      <th>refYear</th>
      <th>validUntil</th>
      <th>compliance</th>
      <th>subType</th>
      <th>dataSources</th>
      <th>regNo</th>
      <th>regAuthority</th>
      <th>nodeid</th>
      <th>uri</th>
      <th>dsType</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[de, en]</td>
      <td>Concrete C25/30 XC4 XF1 XA1 F3 16 L ECOPact, M...</td>
      <td>0c407bf2-c1f2-4a26-ace4-9c0fb1117e3f</td>
      <td>00.01.001</td>
      <td>DE</td>
      <td>02 Bauprodukte / Normal- Leicht- und Porenbeto...</td>
      <td>IBUCategories</td>
      <td>EPD</td>
      <td>2023</td>
      <td>2028</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': '28d74cc0-db8b-4d7e-bc44-5f6d56ce0c4...</td>
      <td>IBU-HOL-HOL-2204100-DE2023000014-1SUG001-DE</td>
      <td>{'uuid': 'd111dbec-b024-4be5-86c5-752d6eb2cf95'}</td>
      <td>IBU_DATA</td>
      <td>https://ibudata.lca-data.com/resource/datastoc...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>1</th>
      <td>[de, en]</td>
      <td>Concrete C25/30 XC4 XF1 XA1 F3 16 L ECOPactR, ...</td>
      <td>24484f95-db48-4d40-9b06-c08ab7b4cd51</td>
      <td>00.01.001</td>
      <td>DE</td>
      <td>02 Bauprodukte / Normal- Leicht- und Porenbeto...</td>
      <td>IBUCategories</td>
      <td>EPD</td>
      <td>2023</td>
      <td>2028</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': '28d74cc0-db8b-4d7e-bc44-5f6d56ce0c4...</td>
      <td>IBU-HOL-HOL-2204100-DE2023000026-1SUG001-DE</td>
      <td>{'uuid': 'd111dbec-b024-4be5-86c5-752d6eb2cf95'}</td>
      <td>IBU_DATA</td>
      <td>https://ibudata.lca-data.com/resource/datastoc...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>2</th>
      <td>[de, en]</td>
      <td>Concrete C25/30 XC4 XF1 XA1 F3 16 M ECOPact , ...</td>
      <td>02e4fdcc-84d7-4f19-95e6-e8d19de9d6ec</td>
      <td>00.01.001</td>
      <td>DE</td>
      <td>02 Bauprodukte / Normal- Leicht- und Porenbeto...</td>
      <td>IBUCategories</td>
      <td>EPD</td>
      <td>2023</td>
      <td>2028</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': '28d74cc0-db8b-4d7e-bc44-5f6d56ce0c4...</td>
      <td>IBU-HOL-HOL-2204100-DE2023000046-1SUG001-DE</td>
      <td>{'uuid': 'd111dbec-b024-4be5-86c5-752d6eb2cf95'}</td>
      <td>IBU_DATA</td>
      <td>https://ibudata.lca-data.com/resource/datastoc...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>3</th>
      <td>[de, en]</td>
      <td>Concrete C25/30 XC4 XF1 XA1 F3 16 M ECOPact , ...</td>
      <td>0ebf7111-4d1d-47a9-96cd-2e969f0f96a3</td>
      <td>00.01.001</td>
      <td>DE</td>
      <td>02 Bauprodukte / Normal- Leicht- und Porenbeto...</td>
      <td>IBUCategories</td>
      <td>EPD</td>
      <td>2023</td>
      <td>2028</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': '28d74cc0-db8b-4d7e-bc44-5f6d56ce0c4...</td>
      <td>IBU-HOL-HOL-2204100-DE2023000034-1SUG001-DE</td>
      <td>{'uuid': 'd111dbec-b024-4be5-86c5-752d6eb2cf95'}</td>
      <td>IBU_DATA</td>
      <td>https://ibudata.lca-data.com/resource/datastoc...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>4</th>
      <td>[de, en]</td>
      <td>Concrete C25/30 XC4 XF1 XA1 F3 16 M ECOPact , ...</td>
      <td>037f6652-34a2-4019-a360-535a6334313a</td>
      <td>00.01.001</td>
      <td>DE</td>
      <td>02 Bauprodukte / Normal- Leicht- und Porenbeto...</td>
      <td>IBUCategories</td>
      <td>EPD</td>
      <td>2023</td>
      <td>2028</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': '28d74cc0-db8b-4d7e-bc44-5f6d56ce0c4...</td>
      <td>IBU-HOL-HOL-2204100-DE2023000039-1SUG001-DE</td>
      <td>{'uuid': 'd111dbec-b024-4be5-86c5-752d6eb2cf95'}</td>
      <td>IBU_DATA</td>
      <td>https://ibudata.lca-data.com/resource/datastoc...</td>
      <td>Process</td>
    </tr>
  </tbody>
</table>
</div>



### Step 2: Retrieve detailed Environmental Product Declaration (EPD) data


```python
# URL to fetch detailed information on EPD
EPD_URL = "https://ibudata.lca-data.com/resource/processes/"

detail_params = {
    "format": "JSON",
    "view": "extended"
}

# Unique identifier for the selected concrete data entry
concrete_uuid = "0ebf7111-4d1d-47a9-96cd-2e969f0f96a3"

# Fetch extended details of the selected concrete mix
response = requests.get(f'{EPD_URL}{concrete_uuid}', headers=headers, params=detail_params)
response.raise_for_status()

detailed_data = response.json()

# Display available data keys for exploration
list(detailed_data.keys())
```




    ['processInformation',
     'modellingAndValidation',
     'administrativeInformation',
     'exchanges',
     'LCIAResults',
     'otherAttributes',
     'version']



### Step 3: Extract and Print GWP


```python
# Access the first LCIA (Life Cycle Impact Assessment) result, which represents GWP-Total
gwp_result = detailed_data["LCIAResults"]["LCIAResult"][0]

# Extract short description of the impact category (e.g., "GWP-total")
gwp_description = gwp_result["referenceToLCIAMethodDataSet"]["shortDescription"][0]["value"]
print(gwp_description)

# Extract unit of measurement for GWP (NOTE index 0 is always the description)
gwp_units = gwp_result["other"]["anies"]
unit_description = gwp_units[0]["value"]["shortDescription"][0]["value"]


# Calculate the total GWP value
total_gwp_value = sum(float(entry["value"]) for entry in gwp_units[1:])
print(f"\nTotal GWP: {total_gwp_value:.5f} {unit_description}")

```

    Global Warming Potential - total (GWP-total)
    
    Total GWP: 166.19997 kg CO_(2) eqv.


### Step 4: Retrieve Reference Flow Properties


```python
# Find reference flow index
reference_flow_idx = detailed_data["processInformation"]["quantitativeReference"]["referenceToReferenceFlow"][0]

# Extract relevant flow properties
flow_properties = detailed_data["exchanges"]["exchange"][reference_flow_idx]["flowProperties"]

# Fetch flow properties lookup table
flow_properties_url = "https://data.eco-platform.org/resource/flowproperties"
flow_properties_response = requests.get(flow_properties_url, headers=headers, params={'format': 'JSON'})
flow_properties_lookup = pd.DataFrame(flow_properties_response.json()["data"])

# Process and display flow properties
processed_flow_properties = []
for prop in flow_properties:
    # Look up the property details using UUID
    lookup_result = flow_properties_lookup.loc[flow_properties_lookup["uuid"] == prop["uuid"]]
    
    # Convert lookup result to dictionary format
    lookup_data = {key: list(value.values())[0] for key, value in lookup_result.to_dict().items()}
    print(lookup_data["name"], prop["meanValue"], lookup_data["defUnit"])
    
    processed_flow_properties.append({
        "name": lookup_data["name"],
        "value": prop["meanValue"],
        "unit": lookup_data["defUnit"]
    })

print("\nHas a GWP of:")
print(f"{total_gwp_value:.5f} {unit_description}")
```

    Mass 2310.0 kg
    Volume 1.0 m3
    
    Has a GWP of:
    166.19997 kg CO_(2) eqv.


### Step 5: Calculate GWP for 1 Ton of Concrete


```python
# Extract mass property for scaling calculations
mass_property = next(fp for fp in processed_flow_properties if fp["name"] == "Mass")
conversion_factor = mass_property["value"] / 1000  # Convert from kg to tons

# Compute the total GWP for 1 ton of concrete
print(f"That means 1 ton (1000 kg) of concrete has a GWP of {(total_gwp_value / conversion_factor):.3f} {unit_description}")

```

    That means 1 ton (1000 kg) of concrete has a GWP of 71.948 kg CO_(2) eqv.


!!Still work in progress!!

### First request


```python
import json
import requests
import pandas as pd


# Base URL of the Aggregator 
base_url = 'https://data.eco-platform.org/resource/'

# Endpoint for processes
endpoint = 'processes'

# Query parameters
params = {
    'search': 'true',
    'distributed': 'true',
    'virtual': 'true',
    'metaDataOnly': 'false',
    'validUntil': '2024',
    'pageSize': 5,
    'startIndex': 0,
    'format': 'JSON',
}
headers = {
    'Authorization': f'Bearer {TOKEN}'
}



# Send GET request
response = requests.get(f'{base_url}{endpoint}', headers=headers, params=params)
response.raise_for_status() 

df = pd.DataFrame(response.json()["data"])
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>languages</th>
      <th>name</th>
      <th>uuid</th>
      <th>version</th>
      <th>geo</th>
      <th>classific</th>
      <th>classificId</th>
      <th>classificSystem</th>
      <th>type</th>
      <th>refYear</th>
      <th>validUntil</th>
      <th>compliance</th>
      <th>subType</th>
      <th>dataSources</th>
      <th>regNo</th>
      <th>regAuthority</th>
      <th>nodeid</th>
      <th>uri</th>
      <th>dsType</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[en]</td>
      <td>Birsta S N2 W2 c/c 1 m</td>
      <td>18f4602a-6d0d-46ff-8701-9402b8e70e6f</td>
      <td>00.05.001</td>
      <td>CN</td>
      <td>Bygg / Stål-, aluminiumskonstruksjoner</td>
      <td>992467c5-eec4-4d69-88ba-672f1a896481</td>
      <td>EPDNorge</td>
      <td>EPD</td>
      <td>2024</td>
      <td>2029</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': 'b497a91f-e14b-4b69-8f28-f50eb157676...</td>
      <td>NEPD-6941-6333-EN</td>
      <td>{'uuid': '7008caa3-6fea-4ad2-81eb-f2b7a0610e6c'}</td>
      <td>EPD-NORWAY_DIGI</td>
      <td>https://epdnorway.lca-data.com/resource/datast...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>1</th>
      <td>[no]</td>
      <td>NaN</td>
      <td>ac58a83c-e898-4505-9d93-9c5b4e3e852f</td>
      <td>00.04.000</td>
      <td>NO</td>
      <td>Bygg / Betongvarer</td>
      <td>f107d200-244c-4c4b-9a14-0c2264316bc2</td>
      <td>EPDNorge</td>
      <td>EPD</td>
      <td>2024</td>
      <td>2029</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': 'b497a91f-e14b-4b69-8f28-f50eb157676...</td>
      <td>NEPD-8487-8141-NO</td>
      <td>{'uuid': '7008caa3-6fea-4ad2-81eb-f2b7a0610e6c'}</td>
      <td>EPD-NORWAY_DIGI</td>
      <td>https://epdnorway.lca-data.com/resource/datast...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>2</th>
      <td>[no]</td>
      <td>NaN</td>
      <td>10eba8d0-fcd6-4c80-9227-69de52a68740</td>
      <td>00.03.000</td>
      <td>IT</td>
      <td>Bygg / Stål-, aluminiumskonstruksjoner</td>
      <td>992467c5-eec4-4d69-88ba-672f1a896481</td>
      <td>EPDNorge</td>
      <td>EPD</td>
      <td>2025</td>
      <td>2030</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>generic dataset</td>
      <td>[{'uuid': 'b497a91f-e14b-4b69-8f28-f50eb157676...</td>
      <td>NEPD-8650-8317</td>
      <td>{'uuid': '7008caa3-6fea-4ad2-81eb-f2b7a0610e6c'}</td>
      <td>EPD-NORWAY_DIGI</td>
      <td>https://epdnorway.lca-data.com/resource/datast...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>3</th>
      <td>[en]</td>
      <td>ABT Skanska Grön AsfaltBio Zero. Gällivare, L...</td>
      <td>58871b96-bcdf-4438-b82f-d100c1df1fe5</td>
      <td>00.03.002</td>
      <td>SE</td>
      <td>Bygg / Asfalt og pukk</td>
      <td>67dd3145-6d07-4939-8373-297d512803ca</td>
      <td>EPDNorge</td>
      <td>EPD</td>
      <td>2021</td>
      <td>2026</td>
      <td>[{'name': 'EN 15804', 'uuid': 'b00f9ec0-7874-1...</td>
      <td>specific dataset</td>
      <td>[{'uuid': 'b497a91f-e14b-4b69-8f28-f50eb157676...</td>
      <td>NEPD-2820-1517</td>
      <td>{'uuid': '7008caa3-6fea-4ad2-81eb-f2b7a0610e6c'}</td>
      <td>EPD-NORWAY_DIGI</td>
      <td>https://epdnorway.lca-data.com/resource/datast...</td>
      <td>Process</td>
    </tr>
    <tr>
      <th>4</th>
      <td>[en]</td>
      <td>ABb PMB, Skanska Industrial Solutions, Dalby ...</td>
      <td>94506cde-817c-4307-bef0-4a317b894e95</td>
      <td>00.07.000</td>
      <td>SE</td>
      <td>Bygg / Asfalt og pukk</td>
      <td>67dd3145-6d07-4939-8373-297d512803ca</td>
      <td>EPDNorge</td>
      <td>EPD</td>
      <td>2024</td>
      <td>2029</td>
      <td>[{'name': 'EN 15804+A2', 'uuid': 'c0016b33-8cf...</td>
      <td>specific dataset</td>
      <td>[{'uuid': 'b497a91f-e14b-4b69-8f28-f50eb157676...</td>
      <td>NEPD-5731-5030-EN</td>
      <td>{'uuid': '7008caa3-6fea-4ad2-81eb-f2b7a0610e6c'}</td>
      <td>EPD-NORWAY_DIGI</td>
      <td>https://epdnorway.lca-data.com/resource/datast...</td>
      <td>Process</td>
    </tr>
  </tbody>
</table>
</div>



In the example above, we sent our first request to the ECO Platform API and got a list of five processes back. We then loaded this response data into a Pandas DataFrame and showed the table.

You can change the parameters, try different endpoints, or further process the data using Pandas. For more details, check the full [API documentation](https://bitbucket.org/okusche/soda4lca/src/7.x-branch/Doc/src/Service_API/Service_API_Datasets_GET.md).

### Filter by **compliance**


```python
# add complianceMode


params["complianceMode"] = "OR"
params["compliance"] = ["EN15804+A1",  "+A2 EF 3.0"]	
res = requests.get(f'{base_url}{endpoint}', headers=headers, params=params)
res.raise_for_status() 

del params['complianceMode']

df = pd.DataFrame(res.json()["data"])
df
```


    ---------------------------------------------------------------------------

    HTTPError                                 Traceback (most recent call last)

    Cell In[10], line 7
          5 params["compliance"] = ["EN15804+A1",  "+A2 EF 3.0"]	
          6 res = requests.get(f'{base_url}{endpoint}', headers=headers, params=params)
    ----> 7 res.raise_for_status() 
          9 del params['complianceMode']
         11 df = pd.DataFrame(res.json()["data"])


    File ~/code/eco-portal-interactive-api-guide/py_env/lib/python3.12/site-packages/requests/models.py:1024, in Response.raise_for_status(self)
       1019     http_error_msg = (
       1020         f"{self.status_code} Server Error: {reason} for url: {self.url}"
       1021     )
       1023 if http_error_msg:
    -> 1024     raise HTTPError(http_error_msg, response=self)


    HTTPError: 500 Server Error:  for url: https://data.eco-platform.org/resource/processes?search=true&distributed=true&virtual=true&metaDataOnly=false&validUntil=2024&pageSize=5&startIndex=0&format=JSON&complianceMode=OR&compliance=EN15804%2BA1&compliance=%2BA2+EF+3.0


In this example, we only chose EPDs that comply with `EN15804+A1` and `+A2 EF 3.0`.

### Select a specific EPD-Number
(I then this is wrong)


```python
params["registrationNumber"] = "EPD-IES"

res = requests.get(f'{base_url}{endpoint}', headers=headers, params=params)
res.raise_for_status()
del params['registrationNumber']

df = pd.DataFrame(res.json()["data"])
df
```


    ---------------------------------------------------------------------------

    HTTPError                                 Traceback (most recent call last)

    Cell In[11], line 4
          1 params["registrationNumber"] = "EPD-IES"
          3 res = requests.get(f'{base_url}{endpoint}', headers=headers, params=params)
    ----> 4 res.raise_for_status()
          5 del params['registrationNumber']
          7 df = pd.DataFrame(res.json()["data"])


    File ~/code/eco-portal-interactive-api-guide/py_env/lib/python3.12/site-packages/requests/models.py:1024, in Response.raise_for_status(self)
       1019     http_error_msg = (
       1020         f"{self.status_code} Server Error: {reason} for url: {self.url}"
       1021     )
       1023 if http_error_msg:
    -> 1024     raise HTTPError(http_error_msg, response=self)


    HTTPError: 500 Server Error:  for url: https://data.eco-platform.org/resource/processes?search=true&distributed=true&virtual=true&metaDataOnly=false&validUntil=2024&pageSize=5&startIndex=0&format=JSON&complianceMode=OR&compliance=EN15804%2BA1&compliance=%2BA2+EF+3.0&registrationNumber=EPD-IES


## Explain the architecture

The ECO Platform has two main parts. The first part collects and combines data and is directly ownd by the ECO Platform. 

The second part consists of the nodes of the EPD Program Operator, which are also connected to the eco platform. 

So far, we have only worked on the Aggregator node.

This diagram explains the relationship.

![Aggregator](./img/ECO_EPD_Portal.004.png)

The Aggregator node does not store data; it only collects data from other nodes and can perform queries on it. It also does not show the complete data. 

If you want to perform more complex queries on a single EPD, you can talk to the node directly to get more detailed information like so.

![full](./img/ECO_EPD_Portal.005.png)

so lets see what we can do on a single node 

### Getting the PDF


```python
lcd = df.iloc[0]

uri = lcd["uri"]
uuid = lcd["uuid"]
version = lcd["version"]
```


```python
print(uri)

# Why??

base_url = uri.split("/")[0] +"//" + uri.split("/")[2] 

url = f"{base_url}/resource/processes/{uuid}/epd?version={version}"
print(url)

# Make the GET request
response = requests.get(url, headers=headers)

# Check if the request was successful
if response.status_code == 200:
    # Save the PDF to a file
    with open("example_process.pdf", "wb") as file:
        file.write(response.content)
    print("PDF downloaded successfully.")
else:
    print(f"Failed to download PDF. Status code: {response.status_code}")

```

    https://data.environdec.com/resource/datastocks/a6c533b3-502e-47b9-885d-31304bf15c64/processes/efe3d139-d74f-46e2-37b4-08dc2e3a4f3f?version=07.01.009
    https://data.environdec.com/resource/processes/efe3d139-d74f-46e2-37b4-08dc2e3a4f3f/epd?version=07.01.009


    PDF downloaded successfully.


### Get CSV


```python
# export https://bitbucket.org/okusche/soda4lca/src/0c04c255287f007249929e9fa80a1ea19f60601b/Doc/src/Service_API/Service_API_Datastock_GET_Export.md
base_url = uri.split("/")[0] +"//" + uri.split("/")[2] 
url = f"{base_url}/resource/datastocks/{uuid}/exportCSV"
print(url)

# Make the GET request
response = requests.get(url , headers=headers)

print(response.text)


# Check if the request was successful
# if response.status_code == 200:
#     # Save the CSV to a file
#     with open("example_process.csv", "wb") as file:
#         file.write(response.content)
#     print("CSV downloaded successfully.")
# else:
#     print(f"Failed to download CSV. Status code: {response.status_code}")
```

    https://data.environdec.com/resource/datastocks/efe3d139-d74f-46e2-37b4-08dc2e3a4f3f/exportCSV
    <!doctype html><html lang="en"><head><title>HTTP Status 501 – Not Implemented</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 501 – Not Implemented</h1><hr class="line" /><p><b>Type</b> Status Report</p><p><b>Message</b> Not Implemented</p><p><b>Description</b> The server does not support the functionality required to fulfill the request.</p><hr class="line" /><h3>Apache Tomcat/9.0.98</h3></body></html>



```python
# convert measurements units



## add gwp local


params = {
    'format': 'JSON'
}

res = requests.get("https://data.eco-platform.org/resource/flowproperties", headers=headers, params=params)
df = pd.DataFrame(res.json()["data"])

item = df.loc[df["uuid"] == "93a60a56-a3c8-11da-a746-0800200b9a66"]
item = {key: list(value.values())[0] for key, value in item.to_dict().items()}
print(item)
df
```

    {'name': 'Mass', 'uuid': '93a60a56-a3c8-11da-a746-0800200b9a66', 'version': '03.00.000', 'classific': 'Technical flow properties', 'defUnitGrp': 'Masseneinheit', 'defUnit': 'kg', 'dsType': 'FlowProperty'}





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
      <th>uuid</th>
      <th>version</th>
      <th>classific</th>
      <th>defUnitGrp</th>
      <th>defUnit</th>
      <th>dsType</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Area</td>
      <td>93a60a56-a3c8-19da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Fläche-Einheit</td>
      <td>qm</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Area*time</td>
      <td>93a60a56-a3c8-21da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of area*time</td>
      <td>m2*a</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Carbon content (biogenic)</td>
      <td>62e503ce-544a-4599-b2ad-bcea15a7bf20</td>
      <td>03.00.003</td>
      <td>Chemical composition of flows</td>
      <td>Units of mass</td>
      <td>kg</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Carbon content (biogenic) - packaging</td>
      <td>262a541b-209e-44cc-a426-33bce30de7b1</td>
      <td>00.01.000</td>
      <td>Chemical composition of flows</td>
      <td>Units of mass</td>
      <td>kg</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Duration</td>
      <td>c0447923-0e60-4b3c-97c2-a86dddd9eea5</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of time</td>
      <td>a</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Energy ren. (net calorific value)</td>
      <td>9b784a67-fbb1-4ad2-8774-30c39643b844</td>
      <td>02.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of energy</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Erneuerbare Primärenergie als Energieträger (P...</td>
      <td>f403679a-8f0c-4945-85ca-15c5d6d9cadf</td>
      <td>25.00.000</td>
      <td>Umweltliche Größen / EPD EN 15804 / LCI Indika...</td>
      <td>Energieeinheit</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Erneuerbare Primärenergie zur stofflichen Nutzung</td>
      <td>67cbf02a-2a4e-4668-941a-b4209c30b3c6</td>
      <td>20.00.000</td>
      <td>Umweltliche Größen / EPD EN 15804 / LCI Indika...</td>
      <td>Units of energy</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Goods transport (mass*distance)</td>
      <td>838aaa20-0117-11db-92e3-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of mass*length</td>
      <td>t*km</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>9</th>
      <td>Gross calorific value</td>
      <td>93a60a56-a3c8-14da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of energy</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Length</td>
      <td>838aaa23-0117-11db-92e3-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of length</td>
      <td>m</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>11</th>
      <td>Mass</td>
      <td>93a60a56-a3c8-11da-a746-0800200b9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Masseneinheit</td>
      <td>kg</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>12</th>
      <td>Net calorific value</td>
      <td>93a60a56-a3c8-11da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of energy</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>13</th>
      <td>Nicht erneuerbare Primärenergie zur stoffliche...</td>
      <td>59b3b1e8-813f-4647-81ca-8220ea2a4cdb</td>
      <td>20.00.000</td>
      <td>Umweltliche Größen / EPD EN 15804 / LCI Indika...</td>
      <td>Units of energy</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>14</th>
      <td>Nicht-erneuerbare Primärenergie als Energieträ...</td>
      <td>15aa5df7-7905-4004-9b5f-a5a539c7aaae</td>
      <td>25.00.000</td>
      <td>Umweltliche Größen / EPD EN 15804 / LCI Indika...</td>
      <td>Energieeinheit</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>15</th>
      <td>Normal Volume</td>
      <td>93a60a56-a3c8-13da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of volume</td>
      <td>m3</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Number of pieces</td>
      <td>01846770-4cfe-4a25-8ad9-919d8d378345</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Anzahl-Einheit</td>
      <td>Stück</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>17</th>
      <td>Price</td>
      <td>bcf81c0a-9fe9-4b88-a25b-99e0dd64ddaa</td>
      <td>19.00.000</td>
      <td>Economic flow properties</td>
      <td>NaN</td>
      <td></td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>18</th>
      <td>Radioactivity</td>
      <td>93a60a56-a3c8-17da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Units of radioactivity</td>
      <td>kBq</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>19</th>
      <td>Total nicht erneuerbare Primärenergie (PENRT)</td>
      <td>70097bf8-cd55-4045-af5f-07144a8448ce</td>
      <td>25.00.000</td>
      <td>Umweltliche Größen / EPD EN 15804 / LCI Indika...</td>
      <td>Energieeinheit</td>
      <td>MJ</td>
      <td>FlowProperty</td>
    </tr>
    <tr>
      <th>20</th>
      <td>Volume</td>
      <td>93a60a56-a3c8-22da-a746-0800200c9a66</td>
      <td>03.00.000</td>
      <td>Technical flow properties</td>
      <td>Volumeneinheit</td>
      <td>m3</td>
      <td>FlowProperty</td>
    </tr>
  </tbody>
</table>
</div>




```python
res = requests.get(f"https://data.eco-platform.org/resource/unitgroups/{uuid}", headers=headers, params=params)

print(res.text)

```

    A unit group data set with the uuid 93a60a56-a3c8-19da-a746-0800200c9a66 cannot be found in the database



```python
# uri = df.iloc[0]["uri"]
# params={}
# params['format'] = 'JSON'
# params['view'] = 'extended'


# print(uri)
# res = requests.get(uri, headers=headers, params=params)#, params=params)
#print(res.json().keys())
#print(json.dumps(res.json(), indent=4))

#pd.DataFrame(res.json())
```

exchanges shortDescription is inconsistent use uuid



publicationAndOwnership -> referenceToRegistrationAuthority as example

note the root node only shows an overview and dose not resolve the full data but only has the url and uuid where you can pull the full dataset


```python

#res = requests.get(df.iloc[0]["uri"], headers=headers)#, params=params)
#print(df.iloc[0]["dataSources"][0]["uuid"])

# print(res.json()["LCIAResults"]["LCIAResult"].keys())
# pd.DataFrame(res.json()["LCIAResults"]["LCIAResult"][0])

```


## Amended  


### Python Setup 


You need to have [Python](https://www.python.org/) set up for this notebook to run. 

Just follow the instructions provided [here](https://www.python.org/downloads/). 

We recommend using a [virtual environment](https://docs.python.org/3/library/venv.html), but feel free to use whatever you like.


1. Create and activate a virtual environment:
   ```bash
   python3 -m venv py_env
   source py_env/bin/activate
   ```

2. Install the required packages:
   ```bash
   pip install ipykernel notebook numpy matplotlib pandas pyarrow ipympl
   ```

3. Add the virtual environment to Jupyter:
   ```bash
   python -m ipykernel install --user --name=py_env --display-name "Python (py_env)"
   ```

4. (Optional) Open the project in VSCode:
   ```bash
   code .
   ```

   - Ensure the virtual environment is active inside VSCode.
   - Select the kernel `Python (py_env)` when opening a notebook.

5. If using the integrated terminal in VSCode, activate the environment inside the terminal:
   ```bash
   source py_env/bin/activate
   ```

### Getting API Key

To get started, you'll need an access token. Follow these steps:

1. **Register for an API Key:**  
   - Go to the [registration page](https://data.eco-platform.org/registration.xhtml).
   - Complete the registration process if you haven’t already.

2. **Generate Your Token:**  
   - Scroll to the bottom of the page and click on **`My Profile`**.
   - Navigate to the **`API Key`** section.
   - Generate a new API key and copy it.

3. **Save the Token:**  
   - Create a `.env` file in your project folder.
   - Add the token to the file with the variable name `TOKEN`. The `.env` file should look something like this:  
     ```plaintext
     TOKEN=iY291bnRyeSI6IkR...aWFz
     ```
